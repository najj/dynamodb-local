# dynamodb-local Docker Image

This is a Docker image for working with dynamodb locally. It's a pretty tiny
project, but I'll try to keep it up to date.

## Downloading

You can access the latest copy of this image from Docker Cloud:

```
docker pull commondream/dynamodb-local
```

## Building and running it locally

To build the docker image, simply run:

```
docker build -t dynamodb-local .
```

To run the image you just built, run:

```
docker run -it -p 8000:8000 dynamodb-local
```

## License

The DynamoDB Local project is licensed under terms from Amazon. This project
does not have any relationship with that project.

The source of this project is licensed under the MIT license. See LICENSE
for more information.
