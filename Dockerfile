FROM openjdk:8-jdk-alpine

RUN mkdir /dynamodb && \
  mkdir /dynamodb/local && \
  mkdir /dynamodb/data && \
  cd /dynamodb/local && \
  apk update && \
  apk add curl && \
  /usr/bin/curl -O http://dynamodb-local.s3-website-us-west-2.amazonaws.com/dynamodb_local_latest.tar.gz && \
  tar xvzf dynamodb_local_latest.tar.gz && \
  rm dynamodb_local_latest.tar.gz && \
  apk del curl

CMD ["java", "-Djava.library.path=/dynamodb/local/DynamoDBLocal_lib", "-jar", "/dynamodb/local/DynamoDBLocal.jar", "-sharedDb", "-dbPath", "/dynamodb/data"]
